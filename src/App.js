import React from "react";
import "./App.css";

import {
  addWatchedMovie,
  add,
  removeWatchedMovie,
  getWatchedMovies,
  getAllMovies,
} from "./index.js";

const getMoviesComponents = (movies) => {
  // Should const this
  var components = [];

  movies.forEach(function (movie) {
    // I'd destrucure movie here for just cleaner looking variables
    components.push(
      <div className="all">
        {/* Missing key prop above */}
        <div>
          {/* Missing alt="" */}
          <img src={movie.image} height="100px" />
        </div>
        <span>
          {/* I'd prefer to use a button, especially since this will make the page jump to the top everytime, which is probably not what you want */}
          <a
            className="movie-watched"
            href="#"
            onClick={function () {
              addWatchedMovie(movie.title, movie.comment, movie.image);
            }}
          >
            {movie.title}
          </a>
        </span>
        <br />
        {/* Comment seems like the type of value that could benifit from a p instead of beeing a span which is inline and comments could be long */}
        <span>{movie.comment}</span>
        <br />
        <br />
      </div>
    );
  });

  return components;
};

function getWatchedMoviesComponents(movies) {
  var components = [];

  movies.forEach(function (movie) {
    components.push(
      movie && (
        <div className="watched">
          {/* Missing Key krop above */}
          <div>
            {/* Missing alt="" */}
            <img src={movie.image} height="100px" />
          </div>
          <span>
            {/* Same as above line 17 */}
            <a
              className="movie-watched"
              href="#"
              onClick={function () {
                removeWatchedMovie(movie.title);
              }}
            >
              {movie.title}
            </a>
          </span>
          <br />
          <span>{movie.comment}</span>
          <br />
          <br />
        </div>
      )
    );
  });

  return components;
}

function App(props) {
  return (
    <div className="App">
      {/* Semantics, something should probably use main */}
      <h1>Codest Movies!</h1>
      {/* Don't use multiple H1 */}
      <h1>Add movie!</h1>
      {/** I would put this in a form, there's probably reason why or why not, more keyboard accessbility in forms, would be by goto 
      
      * Instead of this I would wrap (react way I guess) the input with <label> elements 
      * Easier to style just the label to and clean up the markup and do the styling where the styling should be
      * Labels would also mean we don't need the <br /> the HTML will be alot cleaner        

      */}
      <b>
        TITLE:
        <br />
        {/* Input element can take required, first way of checking empty inputs*/}
        <input
          type="text"
          onChange={function (e) {
            title = e.target.value;
          }}
        />
      </b>
      <br />
      <b>
        IMAGE URL:
        <br />
        <input
          type="text"
          onChange={function (e) {
            image = e.target.value;
          }}
        />
      </b>
      <br />
      <b>
        COMMENT:
        <br />
        <input
          type="text"
          onChange={function (e) {
            comment = e.target.value;
          }}
        />
      </b>
      <br />
      {/* Would use button instead, you can do more inside of a button than in a value, I guess button is good if you have to support some dinosauresque stuff, but do they even have JavaScript then?*/}
      <input
        type="button"
        onClick={function (e) {
          add(title, image, comment);
        }}
        value="ADD MOVIE"
      />
      <h1>Watchlist:</h1>
      {getMoviesComponents(getAllMovies())}
      <h1>Already watched:</h1>
      {getWatchedMoviesComponents(getWatchedMovies())}
    </div>
  );
}

// These are only used in app and should belong to the app
var title = "";
var image = "";
var comment = "";

export default App;
